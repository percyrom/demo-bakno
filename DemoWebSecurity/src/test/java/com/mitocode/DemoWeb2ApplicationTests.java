package com.mitocode;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.mitocode.model.Usuario;
import com.mitocode.repo.IUsuarioRepo;

@SpringBootTest
class DemoWeb2ApplicationTests {

	@Autowired
	private IUsuarioRepo usuarioRepo;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Test
	void crearUsuarioTest() {

		Usuario usuario = new Usuario();
		usuario.setId(4);
		usuario.setNombre("percyrom");
		usuario.setClave(bCryptPasswordEncoder.encode("456"));

		usuarioRepo.save(usuario);

//		Usuario retorno = usuarioRepo.findByNombre("codex");
//		System.out.println(retorno.getClave());

		assertTrue(true);
	}

}
