package com.mitocode.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Persona;
import com.mitocode.repo.IPersonaRepo;

@RestController
@RequestMapping("personas")
public class RestDemoController {
	
	@Autowired
	private IPersonaRepo personaRepo;
	
	@GetMapping
	public List<Persona> listar() {
		return personaRepo.findAll();
	}
	
	@PostMapping
	public void insertar(@RequestBody Persona persona) {
		personaRepo.save(persona);
	}
	
	@PutMapping
	public void modificar(@RequestBody Persona persona) {
		personaRepo.save(persona);
	}
	
	@DeleteMapping(value = "/{idPersona}")
	private void eliminar(@PathVariable("idPersona") Integer idPersona) {
		personaRepo.deleteById(idPersona);
	}

}
