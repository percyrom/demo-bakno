package util;

import java.util.Optional;

public class OpcionalExample {

	public static void main(String[] args) {
		String myValue = "Esta es una prueba";
		Optional<String> optionalString = Optional.ofNullable(myValue);
		System.out.println(optionalString);

	}
}

class MyTestClass {
	public MyTestClass() {
		System.out.println("Init()..");
	}
}
