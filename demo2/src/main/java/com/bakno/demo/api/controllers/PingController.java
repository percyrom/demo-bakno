package com.bakno.demo.api.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PingController {

	@PostMapping("/ping")
	public String ping() {
		return "Hola ping";
	}
	
	@GetMapping("/pong")
	public String pong() {
		return "Hola pong";
	}

}
