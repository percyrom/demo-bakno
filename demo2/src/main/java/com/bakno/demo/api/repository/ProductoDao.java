package com.bakno.demo.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bakno.demo.api.models.Producto;

@Repository
public interface ProductoDao extends JpaRepository<Producto, Long> {

}
