package com.bakno.demo.api.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bakno.demo.api.models.Producto;
import com.bakno.demo.api.services.ProductoService;

@RestController
@RequestMapping("/api/producto")
public class ProductoController {
	
	private static final Logger LOG = LoggerFactory.getLogger(ProductoController.class);
	
	@Autowired
	ProductoService productoService;
	
	@GetMapping("/listar")
	public List<Producto> listar() {
		return productoService.findAll();
	}
	
	@PostMapping("/save")
	public Producto save(@RequestBody Producto producto) {
		return productoService.save(producto);
	}
	
	@PutMapping("/update/{id}")
	public void save(@RequestBody Producto producto, @PathVariable Long id) {
		LOG.info("id: " + id);
	}


	

}
