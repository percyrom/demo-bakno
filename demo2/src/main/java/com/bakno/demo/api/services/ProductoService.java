package com.bakno.demo.api.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bakno.demo.api.models.Producto;
import com.bakno.demo.api.repository.ProductoDao;

@Service
public class ProductoService implements IProductoService{
	
	@Autowired
	public ProductoDao productoDao;

	@Override
	public List<Producto> findAll() {
		return (List<Producto>) productoDao.findAll();
	}

	@Override
	public Producto save(Producto producto) {
		return productoDao.save(producto);
	}


}
