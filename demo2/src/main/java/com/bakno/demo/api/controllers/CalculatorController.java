package com.bakno.demo.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bakno.demo.api.services.IOperations;

@RestController
@RequestMapping("/api/calculator")
public class CalculatorController {
	
	@Autowired
	IOperations operation;

	@PostMapping("/action")
	public int action(@RequestParam int numero1, @RequestParam int numero2) {
		return operation.execute(numero1, numero2);
	}

}
