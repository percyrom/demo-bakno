package com.bakno.demo.api.services;

import java.util.List;

import com.bakno.demo.api.models.Producto;


public interface IProductoService {
	
	public List<Producto> findAll();
	public Producto save(Producto producto);
}
