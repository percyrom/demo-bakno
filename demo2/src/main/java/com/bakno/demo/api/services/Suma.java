package com.bakno.demo.api.services;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class Suma implements IOperations {

	@Override
	public int execute(int n1, int n2) {
		
		return n1 + n2;
	}

}
