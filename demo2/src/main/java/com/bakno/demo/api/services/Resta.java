package com.bakno.demo.api.services;

import org.springframework.stereotype.Service;

@Service
public class Resta implements IOperations {

	@Override
	public int execute(int n1, int n2) {
		
		return n1 - n2;
	}

}
